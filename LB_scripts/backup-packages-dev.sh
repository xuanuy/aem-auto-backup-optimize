#!/bin/bash

#sh scripts/create-build-download-curl.sh -s local -n content-lb-daily-backup -g daily_backup -f "/content/hotels,/content/casinos"
sh /data/apps/aem6/LB_scripts/scripts/create-build-download-curl.sh -s dev -n content-hotels-daily-backup -g daily_backup -f "/content/hotels"
sh /data/apps/aem6/LB_scripts/scripts/create-build-download-curl.sh -s dev -n content-casinos-daily-backup -g daily_backup -f "/content/casinos"