OAK_RUNNER=oak-run-1.2.16.jar

AEM_INSTALL_LOCAL=/Volumes/DATA/Runtime/CQ6.1
AEM_INSTALL_DEV=/data/apps/aem6/author
AEM_INSTALL_DEV3=/data/apps/aem6/author

usage() {
    echo "$1 -s <Server environment>"
    echo ""
    echo "Ex: "
    echo "  Run clean up on local: $1 -s local"
    echo ""
    echo "mandatory:"
    echo ""
    echo "  -s <string> Server environment - local|dev|dev3"
    echo ""
    echo "Return values :"
    echo "  0 => OK"
    echo "  1 => KO"
    echo "  2 => error in arguments"
}

while getopts :s:h opt
do  
    case ${opt} in
        s) source=${OPTARG};;
        h) usage;exit 1;;
        \?) echo "Invalid arg";usage;exit 2;;
        esac
done

#Set source params
if [ "$source" = "dev" ]
then
    AEM_INSTALL="${AEM_INSTALL_DEV}"
    SCRIPT_FOLDER=/data/apps/aem6/LB_scripts/scripts
elif [ "$source" = "dev3" ]
then
    AEM_INSTALL="${AEM_INSTALL_DEV3}"
    SCRIPT_FOLDER=/data/apps/aem6/LB_scripts/scripts
elif [ "$source" = "local" ]
then
    AEM_INSTALL="${AEM_INSTALL_LOCAL}"
    SCRIPT_FOLDER=/Volumes/DATA/LB_scripts/scripts
else
    echo "Invalid arg"
    usage
    exit 2
fi

# Stop server
printf "\n\nStop AEM server ..."
output=$(sh $AEM_INSTALL/crx-quickstart/bin/stop)
done_string="Trying to kill the process..."
while [[ $output != *$done_string* ]]
do
    output=$(sh $AEM_INSTALL/crx-quickstart/bin/stop)
done

# Clean up
clean_up(){
    cd $SCRIPT_FOLDER
    printf "\n\nDelete the unreferenced checkpoints ..."
    java -jar $OAK_RUNNER checkpoints $AEM_INSTALL/crx-quickstart/repository/segmentstore
    java -jar $OAK_RUNNER checkpoints $AEM_INSTALL/crx-quickstart/repository/segmentstore rm-unreferenced
    printf "\n\nCompact repository ..."
    java -jar $OAK_RUNNER compact $AEM_INSTALL/crx-quickstart/repository/segmentstore
}
clean_up

# Start server
printf "\n\nStart AEM server ..."
printf "\n\n##################JOB END##################\n\n\n\n\n\n\n\n"
sh $AEM_INSTALL/crx-quickstart/bin/start