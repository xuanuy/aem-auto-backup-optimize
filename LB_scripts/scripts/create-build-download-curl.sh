#!/bin/bash

platform_local="localhost:4502"
platform_dev="172.16.20.114:4502"
platform_dev3="172.16.20.116:4502"

base_auth="lucien:123qwe"
user_local="admin:admin"
user_dev="admin:admin123*#"
user_dev3="admin:admin123*#"

RED=$(tput setaf 1)
WHITE=$(tput setaf 7)
GREEN=$(tput setaf 2)

usage() {
    echo "$1 -s <AEM Source> -d <AEM Dest> -n <package name> -g <package group> -f <list of the JCR node> -b"
    echo "If PACKAGE or GROUP name consist spaces Please double quote (\"\") them. e.g. \"Test Package Name\" or \"Test Group\"."     
    echo ""
    echo "Ex: "
    echo "  Create package : $1 -s local -n test_package -g my_packages -f '/content/hotels,/content/casinos'"
    echo ""
    echo "mandatory:"
    echo ""
    echo "  -s <string> AEM server - local|dev|dev3"
    echo "  -n <string> Package name to create" 
    echo "  -g <string> Group name of the package"
    echo "  -f <string> Filter string (separate with ,)"
    echo ""
    echo "Return values :"
    echo "  0 => OK"
    echo "  1 => KO"
    echo "  2 => error in arguments"
}

while getopts :s:d:n:g:h:f:b opt
do  
    case ${opt} in
        s) source=${OPTARG};;
        d) dest=${OPTARG};;
        n) name=${OPTARG};;
        g) groupName=${OPTARG};;
        f) filters=${OPTARG};;
        h) usage;exit 1;;
        \?) echo "Invalid arg";usage;exit 2;;
        esac
done

#Setting the package and group names to be used in URLs.
packName=${name// /%20}
grpName=${groupName// /%20}

buildFilters(){
    #filter=[{"root":"/content/FR","rules":[]},{"root":"/content/EN","rules":[]}]
    IFS=',' read -a filterArr <<< "${filters}"
    
    filter='filter=['
    
    for i in "${filterArr[@]}"
    do     
       filter=$filter'{"root":"'$i'","rules":[]},'
    done
    
    filter=${filter%?}']'
}

#Set source params
if [ "$source" = "dev" ]
then
    user="${user_dev}"
    platform="${platform_dev}"
elif [ "$source" = "dev3" ]
then
    user="${user_dev3}"
    platform="${platform_dev3}"
elif [ "$source" = "local" ]
then
    user="${user_local}"
    platform="${platform_local}"
else
    exit 2;
fi

package_info=http://${platform}/crx/packmgr/list.jsp?path=/etc/packages/${grpName}/${packName}.zip

delete_package(){
    printf "\n\nDELETE PACKAGE..."
    printf ${package_info}
    curl_output=$(curl -s -u ${user} -X POST http://${platform}/crx/packmgr/service/script.html/etc/packages/${grpName}/${packName}.zip?cmd=delete)
    if [[ $curl_output = *success\":true* ]]
    then
        printf "\n${GREEN}PACKAGE DELETED:${source}:${name}${WHITE}"
    else
        printf "\n${RED}*DELETING PACKAGE ${name} - ERROR*\n>>>> *${curl_output}*${WHITE}"
        end_script
        exit 1
    fi
    curl -s -u ${user}  http://${platform}/etc/packages/${grpName}/"${packName}".zip > /dev/null 2>&1
}

create_package(){
    printf "\n\nCREATE PACKAGE..."
    printf ${package_info}
    curl_output=$(curl -s -u ${user} -X POST http://${platform}/crx/packmgr/service/exec.json?cmd=create -d packageName=${packName} -d groupName=${grpName})
    if [[ $curl_output = *success\":true* ]]
    then
        printf "\n${GREEN}PACKAGE CREATED:${source}:${name}${WHITE}"
    else
        printf "\n${RED}*PACKAGE ${name} EXISTS\n"
    fi
    curl -s -u ${user}  http://${platform}/etc/packages/${grpName}/"${packName}".zip > /dev/null 2>&1
}

update_package(){
    printf "\n\nUPDATE PACKAGE..."
    printf ${package_info}
    buildFilters
    curl_output=$(curl -s -u ${user} -X POST http://${platform}/crx/packmgr/update.jsp -F packageName=${packName} -F groupName=${grpName} -F path=/etc/packages/${grpName}/${packName}.zip -F "_charset_=UTF-8" -F ${filter})
    if [[ $curl_output = *success\":true* ]]
    then
        printf "\n${GREEN}PACKAGE UPDATED:${source}:${name}${WHITE}"
    else
        printf "\n${RED}*UPDATING PACKAGE ${name} - ERROR*\n>>>> *${curl_output}*${WHITE}"
        end_script
        exit 1
    fi
    curl -s -u ${user}  http://${platform}/etc/packages/${grpName}/"${packName}".zip > /dev/null 2>&1
}

build_package(){
    printf "\n\nBUILDING PACKAGE..."
    printf ${package_info}
    curl_output=$(curl -s -u ${user} -X POST http://${platform}/crx/packmgr/service/.json/etc/packages/${grpName}/${packName}.zip?cmd=build)
    if [[ $curl_output = *success\":true* ]]
    then
        printf "\n${GREEN}PACKAGE BUILT:${source}:${name}${WHITE}"
    else
        printf "\n${RED}*BUILDING PACKAGE ${name} - ERROR*\n>>>> *${curl_output}*${WHITE}"
        end_script
        exit 1
    fi
    curl -s -u ${user}  http://${platform}/etc/packages/${grpName}/"${packName}".zip > /dev/null 2>&1
}

download_package(){
    printf "\n\nDOWNLOADING PACKAGE..."
	backup_location=backup/$source/downloads_$(date +"%m_%d_%Y")
	mkdir -p ${backup_location}
    curl_output=$(curl -s -u ${user} http://${platform}/etc/packages/"${grpName}"/"${packName}".zip -o "${backup_location}"/"${name}".zip)
    printf  "\nDownload to: $backup_location$"
    printf  "\nDownload status (empty is DONE): ${curl_output}"
}

printf "\n####################################"
printf "\n#############_PACKMAN_##############"
printf "\n####################################"
printf "\n\nSOURCE:${source}\nDEST:${dest}\nNAME:${name}.zip\n"

end_script(){
    printf "\n\n${WHITE}##################JOB END##################\n\n\n\n\n\n\n\n"
}

#delete_package
create_package
update_package
build_package
download_package
end_script

exit 0
